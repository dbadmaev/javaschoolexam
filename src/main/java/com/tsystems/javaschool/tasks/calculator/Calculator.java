package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        AtomicInteger parCount = new AtomicInteger(0);
        if (!canCalc(statement, parCount)) {
            return null;
        }
        statement = statement.replaceAll("\\s+", "");
        int pars = parCount.get();

        List<String> result = new ArrayList<>();
        strToList(statement, result);

        int curPars = pars;
        for (int i = 0; i < pars; i++) {
            if (!calculatePars(result, curPars)) {
                return null;
            }
            curPars--;
        }
        String res = null;
        if (result.size() > 1) {
            res = calculateExpression(result);
        }
        if (res.equals("ZERO")) {
            return null;
        }
        int rez = (int) (Double.parseDouble(result.get(0)));
        double res2 = Double.parseDouble(result.get(0)) - rez;
        if (res2 == 0) {
            return String.valueOf(rez);
        }
        return result.get(0);
    }

    private boolean calculatePars(List<String> expression, int pars) {
        int curPars = 0;
        int i = 0;
        int start = 0;
        while (curPars != pars) {
            if (i == expression.size()) {
                return false;
            }
            if (expression.get(i).equals("(")) {
                curPars++;
            }
            if (curPars == pars) {
                start = i;
            }
            i++;
        }
        List<String> curExpression = new ArrayList<>();
        while (!expression.get(i).equals(")")) {
            curExpression.add(expression.get(i));
            i++;
        }
        int end = i;
        String value = curExpression.get(0);
        if (curExpression.size() != 1) {
            value = calculateExpression(curExpression);
        }
        if (value.equals("ZERO")) {
            return false;
        }
        List<String> newExpression = new ArrayList<>();
        i = 0;
        while (i != expression.size()) {
            if (i == start) {
                newExpression.add(value);
                i += (end - start) + 1;
                continue;
            }
            newExpression.add(expression.get(i));
            i++;
        }
        expression.clear();
        expression.addAll(newExpression);
        return true;
    }

    public int findSpecSign(List<String> expression) {
        for (int i = 0; i < expression.size(); i++) {
            if (expression.get(i).equals("*") || expression.get(i).equals("/")) {
                return i;
            }
        }
        return -1;
    }

    public int findSign(List<String> expression) {
        for (int i = 0; i < expression.size(); i++) {
            if (expression.get(i).equals("+") || expression.get(i).equals("-")) {
                return i;
            }
        }
        return -1;
    }


    public String calculate(String left, String right, String sign) {
        double leftVal = Double.parseDouble(left);
        double rightVal = Double.parseDouble(right);
        switch (sign) {
            case "*":
                return String.valueOf(leftVal * rightVal);
            case "/":
                return String.valueOf(leftVal / rightVal);
            case "+":
                return String.valueOf(leftVal + rightVal);
            case "-":
                return String.valueOf(leftVal - rightVal);
            default:
                return null;
        }
    }

    public void rebuildExpression(List<String> expression, int index, String result) {
        List<String> newExpression = new ArrayList<>();
        for (int i = 0; i < expression.size(); i++) {
            if (i == index || i == index + 1) {
                continue;
            }
            if (i == index - 1) {
                newExpression.add(result);
                continue;
            }
            newExpression.add(expression.get(i));
        }
        expression.clear();
        expression.addAll(newExpression);
    }


    public String calculateExpression(List<String> expression) {
        int result = findSpecSign(expression);
        while (result != -1) {
            if (!calc(expression, result)) {
                return "ZERO";
            }
            result = findSpecSign(expression);
        }
        result = findSign(expression);
        while (result != -1) {
            if (!calc(expression, result)) {
                return "ZERO";
            }
            result = findSign(expression);
        }
        return expression.get(0);
    }

    private boolean calc(List<String> expression, int result) {
        String left = expression.get(result - 1);
        String right = expression.get(result + 1);
        if (expression.get(result).equals("/") && Double.parseDouble(right) == 0) {
            return false;
        }
        String resultString = calculate(left, right, expression.get(result));
        rebuildExpression(expression, result, resultString);
        return true;
    }

    private boolean canCalc(String statement, AtomicInteger parCount) {
        if (statement == null) {
            return false;
        }

        statement = statement.replaceAll("\\s+", "");
        if (statement.isEmpty()) {
            return false;
        }

        if (statement.contains("..") || statement.contains("++") || statement.contains("--") ||
                statement.contains("**") || statement.contains("//") || statement.contains(",")) {
            return false;
        }

        List<Integer> leftPar = new ArrayList<>();
        List<Integer> rightPar = new ArrayList<>();

        if (statement.contains("(") || statement.contains(")")) {
            int cur = 0;
            int lPar = 0;
            int rPar = 0;
            while (cur < statement.length()) {
                if (statement.charAt(cur) == '(') {
                    leftPar.add(cur);
                    lPar++;
                } else if (statement.charAt(cur) == ')') {
                    rightPar.add(cur);
                    rPar++;
                }
                cur++;
            }
            if (lPar == rPar) {
                parCount.set(lPar);
                for (int i = 0; i < leftPar.size(); i++) {
                    if (leftPar.get(i) > rightPar.get(i)) {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

    private void strToList(String string, List<String> list) {
        for (int i = 0; i < string.length(); i++) {
            StringBuilder temp = new StringBuilder();
            while (i < string.length() && (Character.isDigit(string.charAt(i)) || string.charAt(i) == '.' ||
                    string.charAt(i) == '-')) {
                if (string.charAt(i) == '-') {
                    if (string.charAt(i) == '-' && i == 0) {
                        temp.append(string.charAt(i));
                        i++;
                        continue;
                    } else if (string.charAt(i) == '-' && string.charAt(i - 1) == '(') {
                        temp.append(string.charAt(i));
                        i++;
                        continue;
                    }
                }
                if (temp.length() > 1) {
                    while (i < string.length() && ((Character.isDigit(string.charAt(i)) || string.charAt(i) == '.'))) {
                        temp.append(string.charAt(i));
                        i++;
                    }
                    list.add(temp.toString());
                    temp = new StringBuilder();
                    continue;
                }

                if (temp.length() == 1 && temp.charAt(0) == '-' && i - 2 >= 0 && !Character.isDigit(string.charAt(i - 1))) {
                    if (Character.isDigit(string.charAt(i - 2))) {
                        list.add(temp.toString());
                        temp = new StringBuilder();
                        temp.append(string.charAt(i));
                        list.add(temp.toString());
                        temp = new StringBuilder();
                        i++;
                        continue;
                    }
                }

                if (temp.length() == 1 && !Character.isDigit(string.charAt(i)) && string.charAt(i) != '.') {
                    list.add(temp.toString());
                    temp = new StringBuilder();
                    temp.append(string.charAt(i));
                    list.add(temp.toString());
                    temp = new StringBuilder();
                    i++;
                    continue;
                }
                temp.append(string.charAt(i));
                i++;
            }
            if (temp.length() != 0) {
                list.add(temp.toString());
            }
            temp = new StringBuilder();
            if (i < string.length()) {
                temp.append(string.charAt(i));
                list.add(temp.toString());
            }
        }
    }
}