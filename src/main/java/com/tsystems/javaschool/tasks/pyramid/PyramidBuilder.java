package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public static int[][] buildPyramid(List<Integer> inputNumbers) {
        int Rows = getRows(inputNumbers);
        int Columns = Rows * 2 - 1;
        Collections.sort(inputNumbers);

        int[][] pyramid = new int[Rows][Columns];
        int insertElement = inputNumbers.size() - 1;
        int passed = 0;

        for (int i = Rows - 1; i >= 0; i--) {
            int elementsInRow = i + 1;
            int insertIndex = Columns - passed - 1;

            while (elementsInRow > 0) {
                pyramid[i][insertIndex] = inputNumbers.get(insertElement);
                insertElement--;
                elementsInRow--;
                insertIndex -= 2;
            }
            passed++;
        }
        return pyramid;
    }

    private static int getRows(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        int Rows = 1;
        int numbers = inputNumbers.size();
        while (numbers >= Rows) {
            numbers -= Rows;
            Rows++;
        }
        Rows--;

        if (numbers != 0) {
            throw new CannotBuildPyramidException();
        }
        return Rows;
    }

}