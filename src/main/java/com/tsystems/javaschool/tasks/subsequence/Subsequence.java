package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public static boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        if (x.isEmpty()) {
            return true;
        }

        int cur = 0;
        while (cur < x.size()) {
            if (y.contains(x.get(cur))) {
                int targetIndex = y.indexOf(x.get(cur));

                while (y.size() > 0 && targetIndex >= 0) {
                    y.remove(0);
                    targetIndex--;
                }
                if (targetIndex >= 0) {
                    return false;
                }
            } else {
                return false;
            }
            cur++;
        }
        return cur == x.size();
    }
}